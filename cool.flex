/*
 *  The scanner definition for COOL.
 */

/*
 *  Stuff enclosed in %{ %} in the first section is copied verbatim to the
 *  output, so headers and global definitions are placed here to be visible
 * to the code in the file.  Don't remove anything that was here initially
 */
%{
#include <cool-parse.h>
#include <stringtab.h>
#include <utilities.h>
#include <stdio.h>
#include <string.h>

/* The compiler assumes these identifiers. */
#define yylval cool_yylval
#define yylex  cool_yylex

/* Max size of string constants */
#define MAX_STR_CONST 1025
#define YY_NO_UNPUT   /* keep g   happy */

extern FILE *fin; /* we read from this file */

/* define YY_INPUT so we read from the FILE fin:
 * This change makes it possible to use this scanner in
 * the Cool compiler.
 */
#undef YY_INPUT
#define YY_INPUT(buf,result,max_size) \
	if ( (result = fread( (char*)buf, sizeof(char), max_size, fin)) < 0) \
		YY_FATAL_ERROR( "read() in flex scanner failed");

char string_buf[MAX_STR_CONST]; /* to assemble string constants */
char *string_buf_ptr;

extern int curr_lineno;
extern int verbose_flag;

extern YYSTYPE cool_yylval;

/*
 *  Add Your own definitions here
 */

%}
%Start COMMENT_SECTION
/*
 * Define names for regular expressions here.
 */

DARROW          =>

%%

[ \n\f\r\t\v]  ; 
 /*
  *  Nested comments
  */
--.* 	;
"(*".*"*)" ;
"(*" 	{ BEGIN COMMENT_SECTION; }
"*)"	{ BEGIN 0; }	
<COMMENT_SECTION>.*"*)".* { BEGIN 0; }
<COMMENT_SECTION>.* ;
 /*
  *  The multiple-character operators.
  */
{DARROW}		{ return (DARROW); }


 /*
  * Keywords are case-insensitive except for the values true and false,
  * which must begin with a lower-case letter.
  */

t[rR][uU][eE] { cool_yylval.boolean = true;
		return (BOOL_CONST); }

f[aA][lL][sS][eE] { cool_yylval.boolean = false;
			return (BOOL_CONST); }


[cC][lL][aA][sS][sS]	{ return (CLASS); 	}
[eE][lL][sS][eE]	{ return (ELSE);	}

[fF][iI]	{ return (FI);		}
[iI][fF]	{ return (IF);		}

[iI][nN]	{ return (IN);		}
[iI][nN][hH][eE][rR][iI][tT][sS] { return (INHERITS); 	}
[iI][sS][vV][oO][iI][dD]	{ return (ISVOID);	}
[lL][eE][tT]	{ return (LET);		}
[lL][oO][oO][pP]	{ return (LOOP);	}
[pP][oO][oO][lL]	{ return (POOL);	}
[tT][hH][eE][nN]	{ return (THEN);	}
[wW][hH][iI][lL][eE]	{ return (WHILE);	}

[cC][aA][sS][eE]	{ return (CASE);	}
[eE][sS][aA][cC]	{ return (ESAC);	}

[oO][fF]	{ return (OF);		}
[nN][eE][wW]	{ return (NEW);		}

[nN][oO][tT] 	{ return (NOT);		}


\"[^"\n]*\" {	
	char *result = new char[strlen(yytext)];
	int escape = 0;
	int result_size = 0;
	for(char *p = yytext; *p != 0; p  ) {
		switch(escape) {
			case 0:
				if(*p == '\\') {
					escape = 1;
				} else {
					result[result_size] = *p;
					result_size  ;
				}
				break;
			case 1:
				if(*p == 'n') {
					result[result_size] = '\n';
					escape = 0;		
				}
				else if(*p == 't') {
					result[result_size] = '\t';
					escape = 0;		
				}
				else if(*p == 'b') {
					result[result_size] = '\b';
					escape = 0;		
				}
				else if(*p == 'f') {
					result[result_size] = '\f';
					escape = 0;		
				}
				else {
					result[result_size] = *p;
				}
				result_size  ;
				break;
		}
	}
	char *result2 = new char[result_size];
	strncpy(result2, result 1, result_size-2);
	cool_yylval.symbol = inttable.add_string(result2);
		return (STR_CONST); 
	}

" "	{ return ' '; }
"-"	{ return '-'; }
"*"	{ return '*'; }
"/"	{ return '/'; }
"~"	{ return '~'; }
"<"	{ return '<'; }
">"	{ return '>'; }
"<="	{ return (LE); }
"["	{ return '['; }
"]"	{ return ']'; }
"="	{ return '='; }
"{"	{ return '{'; }
"}"	{ return '}'; }
";"	{ return ';'; }
"("	{ return '('; }
")"	{ return ')'; }
":"	{ return ':'; }
","	{ return ','; }
"<-"	{ return (ASSIGN); }
\"    	{ return '"'; }
"'"	{ return '\''; }
"."	{ return '.'; }
"\\"	{ return '\\'; }


[0-9]  {cool_yylval.symbol = inttable.add_string(yytext);  
	return (INT_CONST); }
[A-Z][a-zA-Z0-9_]* { cool_yylval.symbol = inttable.add_string(yytext);
			return (TYPEID); }
[a-z][a-zA-Z0-9_]* { 	curr_lineno = yylineno;
			cool_yylval.symbol = inttable.add_string(yytext);
			return (OBJECTID); }
 /*
  *  String constants (C syntax)
  *  Escape sequence \c is accepted for all characters c. Except for 
  *  \n \t \b \f, the result is c.
  *
  */

%%